var mysql = require('mysql');
const chalk = require('chalk');
require('dotenv').config();

var config = {
    host: "100.26.185.48",
    user: "admin",
    password: "admin",
    database: "siap",
    dateStrings: true
};

var con = mysql.createPool(config);

con.getConnection(function(err) {
    if (err){
        console.log(process.env.DB_HOST);
        console.log(err);
        return;
    }else{
        console.log('[',chalk.green('OK'),'] BD siapd conected');
    }
});
module.exports = con;